//Factory: Factoria tipo singleton realizada para una clase base simple
//(c) Maria Torralba
//

#pragma once
#include <functional>
#include <unordered_map>


/******************************************************************************************

C L A S E   B A S E 

Soporte para Factory
******************************************************************************************/
	class Element;
class BaseClass {
protected:
	uint64_t idComponent = 0; //identificador de Componente para las clases derivadas

	//Constructor privado, no se puede instanciar BaseClass
	BaseClass() = default;
public:
	virtual ~BaseClass() = default;

	//Obtiene el ID del componente
	uint64_t GetIDComponent() { return idComponent; };

	//Funcion virtual pura -> Todos los componentes deberian asociarse a un elemento
	virtual void setParentElement(Element *t ) = 0;


};




/******************************************************************************************

 F A C T O R Y

 Factoria de BaseClass programada como singleton
*******************************************************************************************/
class Factory{

	//Mapa para registrar las clases producidas
	std::unordered_map <std::string, std::function<BaseClass*()>> classRegister;

	//Registro de clase en el map
	void MapClass(std::string const & name, std::function<BaseClass*()> const & function);


	//Constructor privado -> SINGLETON
	Factory() = default;
public:
	~Factory() = default;

	//Clase de soporte de auto registro, template
	template<typename Type>
	void FactoryRegister(std::string const &name) {
		SingletonFactory().MapClass(name, []() {
			return new Type ();
		});
	}

	//Getter de una instancia de una clase registrada en Factory
	BaseClass * GetInstanceClass(std::string const & name);

	//Obtencion del singleton de Factoria como static para garantizar el orden de construccion -> tan pronto se necesite, se construira 
	static Factory & SingletonFactory();
};



/******************************************************************************************

C L A S S   R E G I S T E R

Template que facilita el registro de las clases
*******************************************************************************************/
template <typename t>
class ClassRegister {
public:
	ClassRegister(std::string const & name) {
		Factory::SingletonFactory().FactoryRegister<t>(name);
	}
};

//Macro para facilitar el registro de clase -> Enmascara el template
#define Register(x) ClassRegister<x> REG##x (#x); 
