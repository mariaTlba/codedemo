//Factory: Factoria tipo singleton realizada para una clase base simple
//(c) Maria Torralba
//

#include "Factory.h"
#include <string>

void Factory::MapClass(std::string const & name, std::function<BaseClass *()>const & function){
	if (classRegister.find(name) == classRegister.end())
		classRegister[name] = function;
}

BaseClass* Factory::GetInstanceClass(std::string const & name){
	auto search = classRegister.find(name);
	if (search != classRegister.end())
		return search->second();
	return nullptr;
}

Factory & Factory::SingletonFactory(){
	static Factory MyFactory;
	return MyFactory;
}
