//FactoryExampleClass1: Diseño ejemplo de una clase utilizable con nuestro Factory
//(c) Maria Torralba
//

#pragma once
#include <string>
#include "Factory.h"


/**
CLASS CompCollider: Componente encargado de los colliders del elemento al cual se adhiere
*/
	class  CollisionBody;
	class Element;
class CompCollider : public BaseClass {
	CollisionBody * collider = {}; //Colisionadores del elemento
	Element * parent = nullptr; //Identifica el padre del Componente o elemento asociado


public:
	//Identificador de CompCollider = 2;
	CompCollider() { idComponent = 2u; }
	virtual ~CompCollider() = default;

	//Asociacion a un elemento
	virtual void setParentElement(Element *t) override { parent = t; }

	void SetCollider(CollisionBody * cB) { collider = cB; }
	CollisionBody* GetCollider() { return collider; }
};

