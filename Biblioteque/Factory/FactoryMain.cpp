//FactoryMain: Main como ejemplo de uso para probar las diferentes utilidades de Factory
//(c) Maria Torralba
//

#include "Factory.h"
#include <iostream>
#include <string>


/*************************************
EJEMPLO DE USO
**************************************/
int FactoryMain(){

	//"Input de name"
	std::string name = "CompElement";

	//Obtencion de una instancia de la clase identificada con name -> CompElement
	auto instance = Factory::SingletonFactory().GetInstanceClass(name);

	name = "CompCollider";
	//Obtencion de una instancia de la clase identificada con name -> CompCollider
	auto instance2 = Factory::SingletonFactory().GetInstanceClass(name);

	
	delete instance;
	delete instance2;
	return 0;

}

