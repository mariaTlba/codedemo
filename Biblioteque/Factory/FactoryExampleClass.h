//FactoryExampleClass: Diseño ejemplo de una clase utilizable con nuestro Factory
//(c) Maria Torralba
//

#pragma once
#include <string>
#include "Factory.h"

/**
CLASS CompElement: Componente de apoyo para introducir informacion especifica al objeto al cual se adhiere
*/
	class Element;
class CompElement : public BaseClass {
	uint64_t family = 0; //tipo de elemento
	bool hasElement = false; //Contiene mas elementos en el
	uint64_t dataExtra = 0;
	Element * parent = nullptr; //Identifica el padre del Componente o elemento asociado

public:
	//Identificador de CompElement = 1;
	CompElement() { idComponent = 1u; };
	virtual ~CompElement() = default;

	//Asociacion a un elemento
	virtual void setParentElement(Element *t) override { parent = t; }


	void SetFamily(uint64_t f) { family = f; };
	void SetHasElement(bool hE) { hasElement = hE; }
	void SetDataExtra(uint64_t dE) { dataExtra = dE; }

	uint64_t GetFamily() { return family; }
	bool GetHasElement() { return hasElement; }
	uint64_t GetDataExtra() { return dataExtra; }

};

