// Biblioteque.cpp ☺:
//Demo de uso para la clase Tween
//(c) Maria Torralba
//

#include "Tweening/TweenMain.h"
#include "Factory/FactoryMain.h"

int main()
{
	//Tween example
	//TweenMain();

	//Factory example
	FactoryMain();

	return 0;
}

