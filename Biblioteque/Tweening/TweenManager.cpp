//Tween Manager: Gestor de recursos de tipo Tweens
//(c) Maria Torralba
//

#include "TweenManager.h"
#include "Tween.h"
#include <string>


//Alta de Tween dentro del vector tweensVector
void TweenManager::Register(TweenBase * t) {
	std::unique_lock<std::recursive_mutex> lock(mutex_tV);
	//Buscamos dentro del vector por si ya esta registrado
	for (TweenBase * tween : tweensVector) {
		if (tween == t) {
			return;
		}
	}
	//Si no, la registramos
	tweensVector.emplace_back(t);
}

//Baja de Tween dentro del vector tweensVector
void TweenManager::Unregister(TweenBase * t) {
	std::unique_lock<std::recursive_mutex> lock(mutex_tV);
	//Buscamos el Tween en el registro
	auto tween = std::find(tweensVector.begin(), tweensVector.end(), t);
	if (tween != tweensVector.end()) {
		//Desregistro
		*tween = tweensVector.back();
		tweensVector.pop_back();
	}
}

//Patron de singleton para el instancia de la clase
TweenManager & TweenManager::SingletonManager() {
	static TweenManager tManager;
	return tManager;
}

//Gestion de la orden de update para cada uno de los Tweens activos
void TweenManager::Update(double deltaTime) {
	std::unique_lock<std::recursive_mutex> lock(mutex_tV);
	for (int i = 0; i < tweensVector.size(); ++i) {
		tweensVector[i]->Update(deltaTime);
	}
}

