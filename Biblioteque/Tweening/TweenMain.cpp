//TweenMain: Main para probar las diferentes utilidades de TweenManager y Tween
//(c) Maria Torralba
//

#include "Tween.h"
#include <iostream>
#include <thread>


//Clase no estandard para probar Tween
struct MyStruct {
	float x = 0.0f;
	float y = 0.0f;

};
//Funcion Interpolate interpretada para MyStruct
template<>
template<>
void Tween<MyStruct>::Interpolate<MyStruct>(MyStruct const & start, MyStruct const & end, MyStruct & refValue, double t) {
	refValue.x = float(start.x + (end.x - start.x) * t); //start * (1.0f - t) + end * t;
	refValue.y = float(start.y + (end.y - start.y) * t); //start * (1.0f - t) + end * t;
}


/*************************************
EJEMPLO DE USO
**************************************/
int TweenMain(){

	//Metodo de utilizacion 1 -> Value es interno a Tween
	Tween<float> t0;
	t0.Start(0.f, 3.f, 5.0);

	//Metodo de utilizacion 2 -> Value es externo a Tween
	float value_t1 = 0;
	Tween<float> t1;


	//Metodo de utilizacion 3 
	//	-> Funciones de update y finish callback
	//	-> Funcion de Easing diferente a standard -> quadraticIn -> Exponencial
	Tween<float> t3;
	t3.Start(6.f, 0.f, 5.0,
		[](float const &value) {std::cout << "Update " << value << "\n"; },
		[] {std::cout << "Finished" << std::endl; },
		quadraticIn);


	//Metodo de utilizacion 3 
	//	-> Funciones de update y finish callback
	//	-> Utilizacion de Tween con clase propia -> Necesaria la especificacion de Interpolacion
	Tween<MyStruct> t4;
	t4.Start({ 0.f,0.f }, { 3.f, 5.f }, 5.0,
		[](MyStruct const &value) {std::cout << "MyStruct Update" << value.x << ", " << value.y << "\n"; },
		[]() {std::cout << "MyStruct Finished" << std::endl; });


	//Metodo de utilizacion 3 
	//	-> THREAD
	//	-> Funciones de update y finish callback
	//	-> Utilizacion de Tween con clase propia -> Necesaria la especificacion de Interpolacion
	std::thread myThread([]() {
		//Dormir 1 segundo, para dar tiempo al bucle de applicacion para que arranque
		std::this_thread::sleep_for(std::chrono::seconds(1));

		//Creacion de Tween -> Registro seguro
		Tween<double> t5;
		t5.Start(1000.0, 5000.0, 5.0,
			[](double const &value) { std::cout << "Update " << std::this_thread::get_id() << ": " << value << "\n"; },
			[] {std::cout << "Finished" << std::this_thread::get_id() << std::endl;
		});

		//Simulacion de trabajo del thread
		std::this_thread::sleep_for(std::chrono::seconds(6));
	});




	//Clock estable para garantizar segundos
	auto prevTime = std::chrono::steady_clock::now();
	std::chrono::nanoseconds deltaTime;


	//B U C L E  D E  A P L I C A C I O N
	//Simulación de bucle de juego (no termina nunca!)
	while (true) {
		//delta Temporal
		auto now = std::chrono::steady_clock::now();
		deltaTime = now - prevTime;
		double delta = deltaTime.count() / 1000000000.0;


		//Orden de actualizacion a TweenManager
		TweenManager::SingletonManager().Update(delta);


		//PRINTS POR PANTALLA varios
		//std::cout << t0.GetValue() << std::endl;
		if (t0.IsFinished() && t1.IsFinished()) {
			t1.Start(3.f, 0.f, value_t1, 5.0);
		}//else
		 //std::cout << t1.GetValue() << std::endl;


		 //Actualizacion temporal
		prevTime = now;
	}

	//Espera del thread (por educacion)
	myThread.join();


	return 0;

}

