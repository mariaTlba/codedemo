//Tween Manager: Gestor de recursos de tipo Tweens
//(c) Maria Torralba
//

#pragma once
#include <vector>
#include <mutex>



/******************************************************************************************

 T W E E N    M A N A G E R

*******************************************************************************************
Clase destinada a la gestion de Tweens. [See Tween.h]
Thread Safe

*/

//Forward declaration de TweenBase, necesaria para poder declarar punteros en TweenManager
	class TweenBase;
//Gestor de Tweens
class TweenManager {
	friend class TweenBase;

	std::vector<TweenBase * > tweensVector;	//vector de Tweens 
	std::recursive_mutex mutex_tV; //mutex de tweensVector

	//Private: para el registro como Singleton
	TweenManager() = default;
	~TweenManager() = default;

	//Alta de Tween dentro del vector tweensVector
	void Register(TweenBase * t);
	//Baja de Tween dentro del vector tweensVector
	void Unregister(TweenBase * t);

public:
	//Patron de singleton para la instancia de la clase
	static TweenManager& SingletonManager();

	//Gestion de la orden de update para cada uno de los Tweens activos
	void Update(double deltaTime);

};


