//Tween: Herramienta genérica de interpolación 
//(c) Maria Torralba
//

#pragma once
#include <cstdint>
#include <functional>
#include "TweenManager.h"
#include <algorithm>
#include <string>

/******************************************************************************************

	T W E E N  

*******************************************************************************************
- Interpolacion a traves del tiempo (determinado por incrementos delta) entre dos valores (inicial a final)
	TEMPLATE -> Cualquier Tipo interpolable o definido como interpolable.

- Thread Safe (Registro / Desregistro en Manager central)
- Diseño RAII: Cada Tween va a gestionar su destruccion y su desconexion de Manager

- Funcionalidades extra:
Callback de Update que se ejecuta a cada iteración de Tween
Callback de Finish que alerta del final de Tween -> proceso terminado


T I P O S  D E  T W E E N: 
1	Tween interno 
		-> El recurso interpolable es miembro de la clase Tween
2	Tween por referencia 
		-> El recurso interpolable es un recurso externo a Tween por medio de una referencia
3	Tween interno con Callback de Update/Finish 
		-> El recurso interpolable es miembro de la clase Tween 
		-> Funcion como miembro que se ejecuta cuando Update/Finish
*/



//CLASE BASE TWEEN - 
// - Informacion esencial de Tween
// - Interfaz interactuable desde TweenManager
class TweenBase {
	friend class TweenManager;

protected:
	bool pause = false;		//FLAG Pausa Tween
	bool finish = true;		//FLAG Final Tween
	double timeLapse = 0.;	//tiempo de vida acumulado
	double duration = 0.;	//Tiempo de vida total del recurso

	TweenBase() = default;
	virtual ~TweenBase() { Unregister();}

	//Alta de Tween respecto al Manager
	void Register() {
		TweenManager::SingletonManager().Register(this);
	}
	//Baja de Tween respecto al Manager
	void Unregister() {
		TweenManager::SingletonManager().Unregister(this);
	}


public:
	//Funcion para Pausar el Tween
	void Pause() { pause = true; }
	//Reactiva un Tween pausado
	void Resume() { pause = false; }
	//Funcion de consulta sobre el estado Terminado de Tween
	bool IsFinished() const { return finish; }
	//Funcion para terminar el Tween
	void Finish() { finish = true; Unregister(); }

	//Funcion actualizacion de Tween -> next step
	virtual void Update(double deltaTime) = 0;
};


//TEMPLATE  DE  TWEEN  
template <typename Type>
class Tween : public TweenBase {
	Type startValue = {};	//Punto de inicio del interpolador
	Type endValue = {};		//Punto final del interpolador
	Type value = {};		//Valor donde interpolar
	Type & refValue;		//Refencia al valor donde interpolar
 
	//Funciones miembro de Update/Finish
	std::function <void(Type const&)> UpdateCallback = [](Type const &) {};
	std::function <void()> FinishCallback = []() {};
	//Funcion para redefinir la funcion temporal
	std::function<double(double)> easing = linear;

	//Funcion template para reprogramar Interpolate en casos complejos (ej/ no Numericos)
	template <typename InterpType>
	void Interpolate(InterpType const & start, InterpType const & end, InterpType & refValue, double t) {
		refValue = InterpType(start + (end - start) * t); //start * (1.0f - t) + end * t;
	}

	//CONSTRUCTORES PRIVADOS -> Para poder reasignar la referencia desde Start segun Tween de tipo 2
	//a Tipo 1: Tween Interno
	Tween(Type const &start, Type const & end, double time, std::function<double(double)> const &easingFunc) : startValue(start), endValue(end), refValue(value), easing(easingFunc) { duration = time; }
	//a Tipo 2: Tween por referencia 
	Tween(Type const &start, Type const & end, Type & val, double time, std::function<double(double)> const &easingFunc) : startValue(start), endValue(end), refValue(val), easing(easingFunc) { duration = time; }
	//a Tipo 3: Tween interno con Callback de Update/Finish 
	Tween(Type const &start, Type const & end, double time, std::function <void(Type const&)> updateCall, std::function <void()> finishCall, std::function<double(double)> const &easingFunc)
		: startValue(start), endValue(end), refValue(value), UpdateCallback(updateCall), FinishCallback(finishCall), easing(easingFunc) { duration = time; }



public:
	//Constructor por defecto accesible mediante la Interfaz publica
	Tween() : refValue(value) {}


	//Starter para Tween como Tipo 1: Tween Interno
	void Start(Type const& start, Type const& end, double time, std::function<double(double)> const &easingFunc = linear) {
		new(this)Tween(start, end, time, easingFunc); //Emplacement New necesario por si el constructor con el que se ha generado el tween es el de Ref2
		finish = false;
		Register();	//Funcion de registro de Tween
	}

	//Starter para Tween como Tipo 2: Tween por referencia
	void Start(Type const& start, Type const& end, Type & val, double time, std::function<double(double)> const &easingFunc = linear) {
		new(this)Tween(start, end, val, time, easingFunc); //Necesario para reasignar la referencia interna
		finish = false;
		Register();	//Funcion de registro de Tween
	}

	//Starter para Tween como Tipo 3: Tween interno con Callback de Update/Finish 
	void Start(Type const& start, Type const& end, double time, std::function <void(Type const&)> updateCall, std::function <void()> finishCall, std::function<double(double)> const &easingFunc = linear) {
		new(this)Tween(start, end, time,updateCall, finishCall, easingFunc); //Necesario para reasignar la referencia interna
		finish = false;
		Register();	//Funcion de registro de Tween
	}

	//Funcion de Update mediante la cual se genera un nuevo paso dentro de la INTERPOLACION segun deltaTime
	virtual void Update(double deltaTime) override {
		if (pause) //Si el Tween esta Pausado, no ejecutamos actualizacion
			return;
	
		timeLapse += deltaTime; //Incremento temporal
		double t = std::min(1.0, timeLapse / duration); //Limitar entre [0,1]
		
		//Llamada a la funcion que realizara la Interpolacion
		Interpolate(startValue, endValue, refValue, easing(t));
		UpdateCallback(refValue); //Llamada al callback Update 


		//Control de final de Tween
		finish = (t >= 1.0);
		if (finish) {
			FinishCallback(); //Llamada al callback Finish 
			Unregister(); //Baja de Tween
		}

	}

	//Getter del valor interpolador
	Type const & GetValue() const {return refValue;}
};


/******************************************

 FUNCIONES DE EASING, EJEMPLOS

*******************************************/

//Easing functions
inline double linear(double t) { return t; }

//Quadratic
inline double quadraticIn(double t) { return t * t; }
inline double quadraticOut(double t) { return -t * (t - 2.0f); }
inline double quadraticInOut(double t) {
	t *= 2.0f;
	if (t < 1.0f)
		return 0.5f*t*t;
	--t;
	return -0.5f*(t*(t - 2) - 1);
}

//Cubic
inline double cubicIn(double t) { return t * t*t; }
inline double cubicOut(double t) {
	--t;
	return t * t*t + 1;
}
inline double cubicInOut(double t) {
	t *= 2.0f;
	if (t < 1.0f)
		return 0.5f*t*t*t;
	t -= 2;
	return 0.5f*(t*t*t + 2);
}
